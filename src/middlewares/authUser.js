const jwt = require('jsonwebtoken');

const generateError = require('../services/generateError');

const authUser = async (req, res, next) => {
  try {
    // Obtenemos el token de la cabecera de la petición.
    const { authorization } = req.headers;

    // Si no hay token lanzamos un error.
    if (!authorization) {
      generateError('Falta la cabecera de autorización', 400);
    }

    // Variable que contendrá la información del token una vez deserializado.
    let userInfo;

    try {
      userInfo = jwt.verify(authorization, process.env.SECRET);
    } catch {
      generateError('Token incorrecto', 401);
    }

    // Agregamos una nueva propiedad (inventada por nosotros) al objeto request.
    req.user = userInfo;

    // Saltamos a la siguiente función controladora.
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = authUser;
