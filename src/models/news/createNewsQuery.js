const getDB = require('../../db/getDB');

const createNewsQuery = async (
  title,
  leadIn,
  body,
  categoryId,
  photo = '',
  userId
) => {
  let connection;

  try {
    connection = await getDB();

    const [news] = await connection.query(
      `INSERT INTO news(title, leadIn, body, categoryId, photo, userId,
        createdAt) VALUES(?, ?, ?, ?, ?, ?, ?)`,
      [title, leadIn, body, categoryId, photo, userId, new Date()]
    );

    return news[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = createNewsQuery;
