const updateNewsQuery = require('../../models/news/updateNewsQuery');
const searchCategory = require('../../models/news/searchCategory');
const getNewsQuery = require('../../models/news/getNewsQuery');

const savePhoto = require('../../services/savePhoto');
const deletePhoto = require('../../services/deletePhoto');
const generateError = require('../../services/generateError');

const updateNews = async (req, res, next) => {
  try {
    let { title, leadIn, body, categoryId } = req.body;
    const { newsId } = req.params;

    // Comprobamos si el usuario envia una imagen.
    let newPhoto;
    if (req.files?.photo) {
      newPhoto = req.files.photo;
    }

    // El user puede modificar cualquiera de los campos, ya sea 1 o los 5
    if (!(title || leadIn || body || categoryId || newPhoto)) {
      generateError('Faltan campos', 400);
    }

    // Obtenemos los datos de la noticia.
    const news = await getNewsQuery(newsId, req.user.id);

    // Si no es el propietario no puede modificar la noticia.
    if (!news.owner)
      generateError('Permisos insuficientes para realizar esta accion.', 403);

    // En caso de que el usuario no haya enviado algún dato, establecemos
    // como valor por defecto lo que exista en la DB.
    title = title ? title : news.title;
    leadIn = leadIn ? leadIn : news.leadIn;
    body = body ? body : news.body;
    categoryId = categoryId ? categoryId : news.categoryId;
    photo = news.photo;

    // Comprobamos que la categoría existe en la base de datos
    await searchCategory(categoryId);

    // De haber una imagen preexistente la borramos para preparar la nueva
    // y guardarla en el registro.
    if (newPhoto) {
      if (photo) await deletePhoto(photo);

      photo = await savePhoto(newPhoto, 300);
    }

    // Actualizamos la DB con los nuevos datos
    await updateNewsQuery(title, leadIn, body, categoryId, photo, news.id);

    res.send({
      status: 'ok',
      message: 'Publicacion actualizada.',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = updateNews;
