const fs = require('fs/promises');
const path = require('path');
const generateError = require('./generateError');

const deletePhoto = async (imgName) => {
  try {
    // Creamos la ruta absoluta al archivo que queremos eliminar.
    const imgPath = path.join(
      __dirname,
      '..',
      '..',
      process.env.UPLOADS_DIR,
      imgName
    );

    try {
      // Intentamos acceder a la imagen mediante el método "access". Este
      // método genera un error si no es posible acceder al archivo.
      await fs.access(imgPath);
    } catch {
      // Si salta un error quiere decir que el archivo no existe así que
      // finalizamos la función.
      return;
    }

    // Eliminamos el archivo del disco.
    await fs.unlink(imgPath);
  } catch (err) {
    console.error(err);
    generateError('Error al eliminar la imagen del servidor', 500);
  }
};

module.exports = deletePhoto;
