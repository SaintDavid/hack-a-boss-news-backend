const fs = require('fs/promises');
const path = require('path');
const sharp = require('sharp');
const { v4: uuid } = require('uuid');
const generateError = require('./generateError');

const savePhoto = async (img, width) => {
    try {
        // Creamos la ruta absoluta al directorio de subida de archivos.
        const uploadsPath = path.join(
            __dirname,
            '..',
            '..',
            process.env.UPLOADS_DIR
        );

        try {
            // Intentamos acceder al directorio de subida de archivos mediante el
            // método "access". Este método genera un error si no es posible acceder
            // al directorio o archivo.
            await fs.access(uploadsPath);
        } catch {
            // Si salta un error quiere decir que el directorio no existe así que lo creamos.
            await fs.mkdir(uploadsPath);
        }

        // Convertimos la imagen en un objeto Sharp para poder redimensionarla.
        const sharpImg = sharp(img.data);

        // Redimensionamos la imagen. Width sería un ancho en píxeles.
        sharpImg.resize(width);

        // Generamos un nombre único para la imagen.
        const imgName = `${uuid()}.jpg`;

        // Generamos la ruta absoluta a la imagen.
        const imgPath = path.join(uploadsPath, imgName);

        // Guardamos la imagen.
        await sharpImg.toFile(imgPath);

        // Retornamos el nombre con el que hemos guardado la imagen.
        return imgName;
    } catch (err) {
        console.error(err);
        generateError('Error al guardar la imagen en disco');
    }
};

module.exports = savePhoto;
